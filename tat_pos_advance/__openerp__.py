##############################################################################
#
#    Darmawan Fatriananda
#    -
#    Copyright (c) 2015 <http://www.telenais.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################
{
    'name': 'Point of Sale Advance',
    'version': '1.0',
    'category': 'Point of Sale',
    'description': """
This module is used for Point Of Sale Custom, Advance
""",
    'author': "Darmawan Fatriananda.",
    'website': "http://www.telenais.com",
    'depends': ['base','web','account', 'point_of_sale'],
    'data': [
        
        'point_of_sale_view.xml',
        'views/report_receipt.xml',
        'pos_sales_person_view.xml',
         'pos_voucher_view.xml',
         'account_journal_view.xml',
        'account_move_view.xml',
        'pos_bank_view.xml',
        'company_view.xml',
        'security/ir.model.access.csv',
        ],
  #  'demo': [],
   # 'test': [],
    'qweb': ['static/src/xml/pos_advance.xml',
                'static/src/xml/pos_assign_salesperson.xml',
    ],
    'installable': True,
    'auto_install': False,
}
