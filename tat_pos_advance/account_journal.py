##############################################################################
#
#    Darmawan Fatriananda
#    -
#    Copyright (c) 2015 <http://www.telenais.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv


class account_journal(osv.Model):
    _inherit = 'account.journal'
    _columns = {
        'is_card_payment':fields.boolean('Card Payment'),
        'is_voucher_payment':fields.boolean('Voucher Payment'),
        'default_debit_voucher_account_id': fields.many2one('account.account','Default Debit Account (For Voucher Amount)', domain="[('type', '<>', 'view'),('type', '<>', 'consolidation')]",),
        'default_credit_voucher_account_id': fields.many2one('account.account','Default Credit Account (For Voucher Amount)', domain="[('type', '<>', 'view'),('type', '<>', 'consolidation')]",),
    }
account_journal()