##############################################################################
#
#    Darmawan Fatriananda
#    -
#    Copyright (c) 2015 <http://www.telenais.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID
from openerp.tools.translate import _
import time
from datetime import datetime
from openerp import tools
import openerp.addons.decimal_precision as dp

class pos_order(osv.osv):
    _inherit = 'pos.order'

    def _amount_all(self, cr, uid, ids, name, args, context=None):
        res = super(pos_order, self)._amount_all(cr, uid, ids, name, args, context=None)
        for order in self.browse(cr, uid, ids, context=context):
            #added for discount amt
            discount_amount = order.total_discount_amount or 0;
            #end disc amt
            res[order.id]['amount_total'] = (res[order.id]['amount_total']) - discount_amount
        return res

    _columns = {
      
        'salesperson_id':fields.many2one('pos.sales.person', 'SalesPerson',),#useless
        'return_ref_number':fields.char('Return Payment Ref',size=38,help="null if normal order, then exist if returned by"),

        'total_discount_amount':fields.float('Total Discount Amount'),
        'voucher_type': fields.selection([('percent', 'Percent'),
                                   ('amount', 'Amount'),
                                    ],
                                  'Voucher Type', ),#useless
        'amount_tax': fields.function(_amount_all, string='Taxes', digits_compute=dp.get_precision('Account'), multi='all'),
        'amount_total': fields.function(_amount_all, string='Total', digits_compute=dp.get_precision('Account'),  multi='all'),
        'amount_paid': fields.function(_amount_all, string='Paid', states={'draft': [('readonly', False)]}, readonly=True, digits_compute=dp.get_precision('Account'), multi='all'),
        'amount_return': fields.function(_amount_all, 'Returned', digits_compute=dp.get_precision('Account'), multi='all'),

        'discount_amount_ids':fields.one2many('pos.order.discount.amount.line','pos_order_id',string="Discount Amount"),
        'voucher_ids' : fields.many2many('pos.voucher', 'pos_order_voucher_rel',
             'pos_order_id', 'voucher_id', 'List Of Voucher',),
        'voucher_line_ids' : fields.many2many('pos.voucher.line', 'pos_order_voucher_line_rel',
             'pos_order_id', 'voucher_line_id', 'List Of Voucher Number',),


    }

    def create(self, cr, uid, values, context=None):
        #print "order: ",values
        new_order_id = super(pos_order, self).create(cr, uid, values, context=context)
        #print "new order id : ",new_order_id
        line_pool = self.pool.get('pos.order.line')
        #print "inherit create pos.order"
        for an_order in self.browse(cr, uid, [new_order_id], context=context):
            for a_line in an_order.lines:
                res_data = line_pool._amount_line_calculation(cr,uid,[a_line.id],context)
                line_pool.write(cr, uid, [a_line.id], {'price_subtotal': res_data[a_line.id]['price_subtotal'],
                                                       'price_subtotal_incl': res_data[a_line.id]['price_subtotal_incl']
                                                       }, context=context)
        return new_order_id

    def _payment_fields(self, cr, uid, ui_paymentline, context=None):
        ##print(" ui_paymentline ",ui_paymentline)
        payment_data = super(pos_order, self)._payment_fields(cr, uid, ui_paymentline, context=context)
        payment_data.update({
            'card_number': ui_paymentline.get('card_number', False) or False,
            'voucher_number': ui_paymentline.get('voucher_number', False) or False,
            'bank_id': ui_paymentline.get('bank_id', False) or False,
        })

        return payment_data;

    def add_payment(self, cr, uid, order_id, data, context=None):
        """Create a new payment for the order"""
        ##print("Copy add payment")
        context = dict(context or {})
        statement_line_obj = self.pool.get('account.bank.statement.line')
        property_obj = self.pool.get('ir.property')
        order = self.browse(cr, uid, order_id, context=context)
        date = data.get('payment_date', time.strftime('%Y-%m-%d'))
        if len(date) > 10:
            timestamp = datetime.strptime(date, tools.DEFAULT_SERVER_DATETIME_FORMAT)
            ts = fields.datetime.context_timestamp(cr, uid, timestamp, context)
            date = ts.strftime(tools.DEFAULT_SERVER_DATE_FORMAT)
        args = {
            'amount': data['amount'],
            'date': date,
            'name': order.name + ': ' + (data.get('payment_name', '') or ''),
            'partner_id': order.partner_id and self.pool.get("res.partner")._find_accounting_partner(order.partner_id).id or False,
        }

        journal_id = data.get('journal', False)
        statement_id = data.get('statement_id', False)
        assert journal_id or statement_id, "No statement_id or journal_id passed to the method!"

        journal = self.pool['account.journal'].browse(cr, uid, journal_id, context=context)
        if journal.is_voucher_payment:
            #print "set amount = 0 if : ",journal.is_voucher_payment
            args.update({
            'amount': 0,
            })
        # use the company of the journal and not of the current user
        company_cxt = dict(context, force_company=journal.company_id.id)
        account_def = property_obj.get(cr, uid, 'property_account_receivable', 'res.partner', context=company_cxt)
        args['account_id'] = (order.partner_id and order.partner_id.property_account_receivable \
                             and order.partner_id.property_account_receivable.id) or (account_def and account_def.id) or False

        if not args['account_id']:
            if not args['partner_id']:
                msg = _('There is no receivable account defined to make payment.')
            else:
                msg = _('There is no receivable account defined to make payment for the partner: "%s" (id:%d).') % (order.partner_id.name, order.partner_id.id,)
            raise osv.except_osv(_('Configuration Error!'), msg)

        context.pop('pos_session_id', False)

        for statement in order.session_id.statement_ids:
            if statement.id == statement_id:
                journal_id = statement.journal_id.id
                break
            elif statement.journal_id.id == journal_id:
                statement_id = statement.id
                break

        if not statement_id:
            raise osv.except_osv(_('Error!'), _('You have to open at least one cashbox.'))

        v_card_number = ''
        if 'card_number' in data :
            v_card_number =  data['card_number']
        v_voucher_number = ''
        if 'voucher_number' in data :
            v_voucher_number =  data['voucher_number']
            if v_voucher_number == 'Discount Amount':
                #print "set amount = 0 if : ",v_voucher_number
                args.update({
                'amount': 0,
                })
        v_bank_id = ''
        if 'bank_id' in data :
            v_bank_id =  data['bank_id']




        args.update({
            'statement_id': statement_id,
            'pos_statement_id': order_id,
            'journal_id': journal_id,
            'ref': order.session_id.name,
            'card_number': v_card_number,
            'voucher_number': v_voucher_number,
            'bank_id': v_bank_id,
        })

        statement_line_obj.create(cr, uid, args, context=context)

        return statement_id
    def _amount_line_tax(self, cr, uid, line, context=None):
        account_tax_obj = self.pool['account.tax']
        taxes_ids = [tax for tax in line.product_id.taxes_id if tax.company_id.id == line.order_id.company_id.id]
        price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
        if line.discount_type == 'amount':
                price = line.price_unit  - (line.discount or 0.0)

        taxes = account_tax_obj.compute_all(cr, uid, taxes_ids, price, line.qty, product=line.product_id, partner=line.order_id.partner_id or False)['taxes']

        val = 0.0
        for c in taxes:
            val += c.get('amount', 0.0)

        return val


    def _order_fields(self, cr, uid, ui_order, context=None):
         order_data = super(pos_order, self)._order_fields(cr, uid, ui_order, context=context)


         return_ref_number = ''
         if 'return_ref_number' in ui_order :
             return_ref_number =  ui_order['return_ref_number']
         voucher_type = ''
         if 'voucher_type' in ui_order :
             voucher_type =  ui_order['voucher_type']
         voucher_ids = []
         if 'voucher_ids' in ui_order :
             voucher_ids =  ui_order['voucher_ids']
         voucher_line_ids = []
         if 'voucher_line_ids' in ui_order :
             voucher_line_ids =  ui_order['voucher_line_ids']

         order_data.update({
            'return_ref_number':  return_ref_number or False,
             'voucher_type': voucher_type or False,
             'voucher_ids': voucher_ids or [],
             'voucher_line_ids': voucher_line_ids or [],

        })

         return order_data;
    def _process_order(self, cr, uid, order, context=None):
        #print "order : ",order
        new_order_id = super(pos_order, self)._process_order(cr, uid, order, context=context)
        #print "new_order_id : ",new_order_id
        discount_amount_line_pool = self.pool.get('pos.order.discount.amount.line')
        pos_voucher_line_pool = self.pool.get('pos.voucher.line')
        ##print "order  :",order

        voucher_ids =  order['voucher_ids'] or []
        voucher_line_ids = order['voucher_line_ids'] or []
        total_discount_amount = 0
        # discount voucher without number
        if voucher_ids:
            for voucher_data in voucher_ids:
                 line_values = {}
                 line_values.update({
                        'pos_voucher_id': voucher_data['id'],
                        'name' : voucher_data['name'] or voucher_data['id'] ,
                        'date' :time.strftime('%Y-%m-%d'),
                        'pos_order_id': new_order_id,

                 })
                 for payment_data in order['statement_ids']:
                     payment_fields = self._payment_fields(cr, uid, payment_data[2], context=context);
                     if payment_fields['voucher_number'] and payment_fields['voucher_number'] == voucher_data['name'] :
                         journal_id = payment_fields['journal']
                         journal_obj = self.pool.get('account.journal').browse(cr,uid,journal_id,context=None)
                         #print "journal obj name : ",journal_obj.name
                         line_values.update({
                            'account_id': journal_obj.default_debit_voucher_account_id.id,
                            'amount' :  payment_fields['amount']
                         })
                         total_discount_amount+=payment_fields['amount']
                 #print "discount line : ",line_values
                 discount_amount_line_pool.create(cr,uid,line_values,context=None)

        #discount voucher with number
        if voucher_line_ids:
             for voucher_line_data in voucher_line_ids:
                 line_values = {}
                 line_values.update({
                        'pos_voucher_line_id': voucher_line_data['id'],
                        'name' : voucher_line_data['name'] or voucher_line_data['id'] ,
                        'date' :time.strftime('%Y-%m-%d'),
                        'pos_order_id': new_order_id,

                 })
                 for payment_data in order['statement_ids']:
                     ##print " payments ",payment_data
                     payment_fields = self._payment_fields(cr, uid, payment_data[2], context=context);
                     ##print " payment_fields ",payment_fields
                     if payment_fields['voucher_number'] and payment_fields['voucher_number'] == voucher_line_data['name'] :
                         journal_id = payment_fields['journal']
                         journal_obj = self.pool.get('account.journal').browse(cr,uid,journal_id,context=None)
                         #print "journal obj name : ",journal_obj.name
                         line_values.update({
                            'account_id': journal_obj.default_debit_voucher_account_id.id,
                            'amount' :  payment_fields['amount']
                         })
                         total_discount_amount+=payment_fields['amount']
                 #print "discount line : ",line_values
                 discount_amount_line_pool.create(cr,uid,line_values,context=None)
                 pos_voucher_line_pool.write(cr,uid,voucher_line_data['id'],{'pos_order_id': new_order_id,'is_use': True},context=None)

        #discount amount :
        if order['statement_ids']:
            for payment_data in order['statement_ids']:
                line_values = {}
                payment_fields = self._payment_fields(cr, uid, payment_data[2], context=context);
                if payment_fields['voucher_number'] and payment_fields['voucher_number'] == 'Discount Amount' :
                    line_values.update({
                        'name' : payment_fields['voucher_number'] ,
                        'date' :time.strftime('%Y-%m-%d'),
                        'pos_order_id': new_order_id,

                    })
                    journal_id = payment_fields['journal']
                    journal_obj = self.pool.get('account.journal').browse(cr,uid,journal_id,context=None)
                    def_account_id = journal_obj.default_debit_account_id.id
                    pos_session_id = order['pos_session_id']
                    config_obj = self.pool.get('pos.session').browse(cr,uid,pos_session_id,context=None).config_id
                    #print "config_obj : ",config_obj
                    account_id  =config_obj.default_debit_discount_amount_account_id and config_obj.default_debit_discount_amount_account_id.id or False
                    #print "journal obj name : ",journal_obj.name
                    line_values.update({
                            'account_id': account_id or def_account_id,
                            'amount' :  payment_fields['amount']
                         })
                    total_discount_amount+=payment_fields['amount']
                    #print "discount line : ",line_values
                    discount_amount_line_pool.create(cr,uid,line_values,context=None)

        if total_discount_amount > 0 and new_order_id:
            self.write(cr,uid,new_order_id,{'total_discount_amount': total_discount_amount},context=None)

        return new_order_id;



    def reprint_receipt(self, cr, uid, ids, context={}):
        if context is None:
            context = {}
        value = self.read(cr, uid, ids)[0]
        if ids:
            datas = {'data_ids':ids,
                     #'form': value,
                     'active_ids':ids
                     }
            
        return self.pool['report'].get_action(cr, uid, ids, 
                        'point_of_sale.report_receipt', 
                        data=datas, context=context)


    #duplicate method :
    def _create_account_move_line(self, cr, uid, ids, session=None, move_id=None, context=None):
        # Tricky, via the workflow, we only have one id in the ids variable
        """Create a account move line of order grouped by products or not."""

        #print "-----------------------"
        #print " _create_account_move_line  "
        account_move_obj = self.pool.get('account.move')
        account_period_obj = self.pool.get('account.period')
        account_tax_obj = self.pool.get('account.tax')
        property_obj = self.pool.get('ir.property')
        cur_obj = self.pool.get('res.currency')

        #session_ids = set(order.session_id for order in self.browse(cr, uid, ids, context=context))

        if session and not all(session.id == order.session_id.id for order in self.browse(cr, uid, ids, context=context)):
            raise osv.except_osv(_('Error!'), _('Selected orders do not have the same session!'))

        grouped_data = {}
        have_to_group_by = session and session.config_id.group_by or False

        def compute_tax(amount, tax, line):
            if amount > 0:
                tax_code_id = tax['base_code_id']
                tax_amount = line.price_subtotal * tax['base_sign']
            else:
                tax_code_id = tax['ref_base_code_id']
                tax_amount = -line.price_subtotal * tax['ref_base_sign']

            return (tax_code_id, tax_amount,)

        for order in self.browse(cr, uid, ids, context=context):
            if order.account_move:
                continue
            if order.state != 'paid':
                continue

            current_company = order.sale_journal.company_id

            group_tax = {}
            account_def = property_obj.get(cr, uid, 'property_account_receivable', 'res.partner', context=context)

            order_account = order.partner_id and \
                            order.partner_id.property_account_receivable and \
                            order.partner_id.property_account_receivable.id or \
                            account_def and account_def.id

            if move_id is None:
                # Create an entry for the sale
                move_id = self._create_account_move(cr, uid, order.session_id.start_at, order.name, order.sale_journal.id, order.company_id.id, context=context)
                #print "darfat : move_id : ",move_id

            move = account_move_obj.browse(cr, uid, move_id, context=context)

            def insert_data(data_type, values):
                # if have_to_group_by:

                sale_journal_id = order.sale_journal.id

                # 'quantity': line.qty,
                # 'product_id': line.product_id.id,
                values.update({
                    'date': order.date_order[:10],
                    'ref': order.name,
                    'partner_id': order.partner_id and self.pool.get("res.partner")._find_accounting_partner(order.partner_id).id or False,
                    'journal_id' : sale_journal_id,
                    'period_id': move.period_id.id,
                    'move_id' : move_id,
                    'company_id': current_company.id,
                })

                if data_type == 'product':
                    key = ('product', values['partner_id'], values['product_id'], values['analytic_account_id'], values['debit'] > 0)
                elif data_type == 'tax':
                    key = ('tax', values['partner_id'], values['tax_code_id'], values['debit'] > 0)
                elif data_type == 'counter_part':
                    key = ('counter_part', values['partner_id'], values['account_id'], values['debit'] > 0)
                else:
                    return

                grouped_data.setdefault(key, [])

                # if not have_to_group_by or (not grouped_data[key]):
                #     grouped_data[key].append(values)
                # else:
                #     pass

                if have_to_group_by:
                    if not grouped_data[key]:
                        grouped_data[key].append(values)
                    else:
                        for line in grouped_data[key]:
                            if line.get('tax_code_id') == values.get('tax_code_id'):
                                current_value = line
                                current_value['quantity'] = current_value.get('quantity', 0.0) +  values.get('quantity', 0.0)
                                current_value['credit'] = current_value.get('credit', 0.0) + values.get('credit', 0.0)
                                current_value['debit'] = current_value.get('debit', 0.0) + values.get('debit', 0.0)
                                current_value['tax_amount'] = current_value.get('tax_amount', 0.0) + values.get('tax_amount', 0.0)
                                break
                        else:
                            grouped_data[key].append(values)
                else:
                    grouped_data[key].append(values)

            #because of the weird way the pos order is written, we need to make sure there is at least one line,
            #because just after the 'for' loop there are references to 'line' and 'income_account' variables (that
            #are set inside the for loop)
            #TOFIX: a deep refactoring of this method (and class!) is needed in order to get rid of this stupid hack
            assert order.lines, _('The POS order must have lines when calling this method')
            # Create an move for each order line

            cur = order.pricelist_id.currency_id
            round_per_line = True
            if order.company_id.tax_calculation_rounding_method == 'round_globally':
                round_per_line = False
            for line in order.lines:
                tax_amount = 0
                taxes = []
                for t in line.product_id.taxes_id:
                    if t.company_id.id == current_company.id:
                        taxes.append(t)
                computed_taxes = account_tax_obj.compute_all(cr, uid, taxes, line.price_unit * (100.0-line.discount) / 100.0, line.qty)['taxes']

                for tax in computed_taxes:
                    tax_amount += cur_obj.round(cr, uid, cur, tax['amount']) if round_per_line else tax['amount']
                    if tax_amount < 0:
                        group_key = (tax['ref_tax_code_id'], tax['base_code_id'], tax['account_collected_id'], tax['id'])
                    else:
                        group_key = (tax['tax_code_id'], tax['base_code_id'], tax['account_collected_id'], tax['id'])

                    group_tax.setdefault(group_key, 0)
                    group_tax[group_key] += cur_obj.round(cr, uid, cur, tax['amount']) if round_per_line else tax['amount']

                amount = line.price_subtotal

                # Search for the income account
                if  line.product_id.property_account_income.id:
                    income_account = line.product_id.property_account_income.id
                elif line.product_id.categ_id.property_account_income_categ.id:
                    income_account = line.product_id.categ_id.property_account_income_categ.id
                else:
                    raise osv.except_osv(_('Error!'), _('Please define income '\
                        'account for this product: "%s" (id:%d).') \
                        % (line.product_id.name, line.product_id.id, ))

                # Empty the tax list as long as there is no tax code:
                tax_code_id = False
                tax_amount = 0
                while computed_taxes:
                    tax = computed_taxes.pop(0)
                    tax_code_id, tax_amount = compute_tax(amount, tax, line)

                    # If there is one we stop
                    if tax_code_id:
                        break

                # Create a move for the line
                insert_data('product', {
                    'name': line.product_id.name,
                    'quantity': line.qty,
                    'product_id': line.product_id.id,
                    'account_id': income_account,
                    'analytic_account_id': self._prepare_analytic_account(cr, uid, line, context=context),
                    'credit': ((amount>0) and amount) or 0.0,
                    'debit': ((amount<0) and -amount) or 0.0,
                    'tax_code_id': tax_code_id,
                    'tax_amount': tax_amount,
                    'partner_id': order.partner_id and self.pool.get("res.partner")._find_accounting_partner(order.partner_id).id or False
                })

                # For each remaining tax with a code, whe create a move line
                for tax in computed_taxes:
                    tax_code_id, tax_amount = compute_tax(amount, tax, line)
                    if not tax_code_id:
                        continue

                    insert_data('tax', {
                        'name': _('Tax'),
                        'product_id':line.product_id.id,
                        'quantity': line.qty,
                        'account_id': income_account,
                        'credit': 0.0,
                        'debit': 0.0,
                        'tax_code_id': tax_code_id,
                        'tax_amount': tax_amount,
                        'partner_id': order.partner_id and self.pool.get("res.partner")._find_accounting_partner(order.partner_id).id or False
                    })

            # Create a move for each tax group
            (tax_code_pos, base_code_pos, account_pos, tax_id)= (0, 1, 2, 3)

            for key, tax_amount in group_tax.items():
                tax = self.pool.get('account.tax').browse(cr, uid, key[tax_id], context=context)
                insert_data('tax', {
                    'name': _('Tax') + ' ' + tax.name,
                    'quantity': line.qty,
                    'product_id': line.product_id.id,
                    'account_id': key[account_pos] or income_account,
                    'credit': ((tax_amount>0) and tax_amount) or 0.0,
                    'debit': ((tax_amount<0) and -tax_amount) or 0.0,
                    'tax_code_id': key[tax_code_pos],
                    'tax_amount': tax_amount,
                    'partner_id': order.partner_id and self.pool.get("res.partner")._find_accounting_partner(order.partner_id).id or False
                })

            # counterpart
            insert_data('counter_part', {
                'name': _("Trade Receivables"), #order.name,
                'account_id': order_account,
                'credit': ((order.amount_total < 0) and -order.amount_total) or 0.0,
                'debit': ((order.amount_total > 0) and order.amount_total) or 0.0,
                'partner_id': order.partner_id and self.pool.get("res.partner")._find_accounting_partner(order.partner_id).id or False
            })


            #discount amount voicer
            if order.discount_amount_ids :
                #print "order : ",order.name
                for discount_amount_obj in order.discount_amount_ids:
                    #print " dicsount amount_ob... ",discount_amount_obj.name
                    disc_line_name = discount_amount_obj.name
                    if discount_amount_obj.pos_voucher_id or discount_amount_obj.pos_voucher_line_id:
                        disc_line_name = 'Discount Voucher'
                    insert_data('counter_part', {
                            'name': disc_line_name,
                        'account_id': discount_amount_obj.account_id and discount_amount_obj.account_id.id,
                        'credit': 0.0,
                        'debit': ((discount_amount_obj.amount > 0) and discount_amount_obj.amount) or 0.0,
                        'partner_id': order.partner_id and  order.partner_id.id or False
                    })



            order.write({'state':'done', 'account_move': move_id})

        all_lines = []
        for group_key, group_data in grouped_data.iteritems():
            for value in group_data:
                all_lines.append((0, 0, value),)
        if move_id: #In case no order was changed
            self.pool.get("account.move").write(cr, uid, [move_id], {'line_id':all_lines}, context=context)

        return True

pos_order()
class pos_session(osv.osv):
    _inherit = 'pos.session'
    def _confirm_orders(self, cr, uid, ids, context=None):
        res_bool = super(pos_session, self)._confirm_orders(cr, uid, ids, context=context)
        #print "Inherit confirm orders..."
        move_line_pool = self.pool.get('account.move.line')
        for session in self.browse(cr, uid, ids, context=context):
            for order in session.order_ids:
                #print "my order : ",order.state
                if order.state == 'done':
                    for statement in order.statement_ids:
                        for move in statement.journal_entry_id:
                            line_ids = []
                            for move_line in move.line_id:
                                line_ids.append(move_line.id)
                            if line_ids:
                                #print "update move line with card number : ",statement.card_number
                                move_line_pool.write(cr, uid, line_ids, {'card_number': statement.card_number,
                                                                         'bank_id': statement.bank_id.id}, context=context)




        return res_bool;
pos_session();

class pos_order_line(osv.osv):
    _inherit = 'pos.order.line'

    _DISCOUNT_TYPE = [('percent','Percent'),('amount','Amount')]

    def _amount_line_calculation(self, cr, uid, ids, context=None):
        res = dict([(i, {}) for i in ids])
        account_tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        for line in self.browse(cr, uid, ids, context=context):
            taxes_ids = [ tax for tax in line.product_id.taxes_id if tax.company_id.id == line.order_id.company_id.id ]
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            if line.discount_type == 'amount':
                price = line.price_unit  - (line.discount or 0.0)

            #print "price ... ",line.id," = ",price
            taxes = account_tax_obj.compute_all(cr, uid, taxes_ids, price, line.qty, product=line.product_id, partner=line.order_id.partner_id or False)

            #print "total : ",taxes['total']
            #print "total_included : ",taxes['total_included']
            cur = line.order_id.pricelist_id.currency_id
            res[line.id]['price_subtotal'] = taxes['total']
            res[line.id]['price_subtotal_incl'] = taxes['total_included']
        return res
    '''def onchange_qty(self, cr, uid, ids, product, discount, discount_type,qty, price_unit, context=None):
        result = {}
        if not product:
            return result
        account_tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')

        prod = self.pool.get('product.product').browse(cr, uid, product, context=context)

        price = price_unit * (1 - (discount or 0.0) / 100.0)
        if discount_type == 'amount' :
            price = price_unit  - (discount or 0.0)

        taxes = account_tax_obj.compute_all(cr, uid, prod.taxes_id, price, qty, product=prod, partner=False)

        result['price_subtotal'] = taxes['total']
        result['price_subtotal_incl'] = taxes['total_included']
        return {'value': result}
'''
    _columns = {
        'salesperson_id':fields.many2one('pos.sales.person', 'SalesPerson',),
        'discount_type'           : fields.selection(_DISCOUNT_TYPE,'Discount Type',required=False),
    }
    _defaults = {
        'user_id': lambda self, cr, uid, context: uid,
        'discount_type': 'percent',
    }


pos_order_line()

class pos_config(osv.osv):
    _inherit = 'pos.config'
    _columns = {
      
        'minimum_discount':fields.float('Minimum Discount  (%) '),
        'password_discount':fields.char('Password',size=18),
        'default_debit_discount_amount_account_id': fields.many2one('account.account','Default Debit Account (For Discount Amount)', domain="[('type', '<>', 'view'),('type', '<>', 'consolidation')]",),
        'default_credit_discount_amount_account_id': fields.many2one('account.account','Default Credit Account (For Discount Amount)', domain="[('type', '<>', 'view'),('type', '<>', 'consolidation')]",),

      
    }
 
pos_config()

class account_bank_statement_line(osv.osv):
    _inherit = 'account.bank.statement.line'
    _columns= {
        'card_number': fields.char('Card Number', size=16),
        'voucher_number': fields.char('Voucher Number', size=80),
        'bank_id': fields.many2one('pos.bank','Bank',),

    }

account_bank_statement_line()
class account_move_line(osv.osv):
    _inherit = 'account.move.line'
    _columns= {
        'card_number': fields.char('Card Number', size=16),
        'voucher_number': fields.char('Voucher Number', size=80),
        'bank_id': fields.many2one('pos.bank','Bank',),
    }

account_move_line()