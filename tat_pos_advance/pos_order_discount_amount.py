from openerp.osv import fields, osv
from openerp import SUPERUSER_ID

class pos_order_discount_amount_line(osv.osv):
    _name = 'pos.order.discount.amount.line'
    _description = 'Pos order discount amount line'
    _columns = {
        'name': fields.char('Ref Number', size=180,required=False),
        'amount':fields.float("Amount"),
	    'date':fields.date("Date"),
        'account_id':fields.many2one('account.account','Account'),
        'pos_voucher_id':fields.many2one('pos.voucher','Voucher Type'),
	    'pos_voucher_line_id':fields.many2one('pos.voucher.line','Voucher Number'),
        'pos_order_id':fields.many2one('pos.order','Order ID'),
        }
    _defaults = {
        'is_use': False,
    }
pos_order_discount_amount_line();