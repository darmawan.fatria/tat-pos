##############################################################################
#
#    Darmawan Fatriananda
#    -
#    Copyright (c) 2015 <http://www.telenais.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################
from openerp.osv import fields, osv
from openerp import SUPERUSER_ID


class pos_voucher(osv.osv):
    _name = 'pos.voucher' 
    _description = 'POS Voucher And Coupon Promotion Configuration'
    _columns = {
        'name': fields.char('Voucher Name', size=80,required=True),
        'description': fields.text('Voucher Description'),
        'date_from':fields.date("Start Date",required=True),
        'date_end':fields.date("Expiry Date",required=True),
        'is_use_number' :fields.boolean("Use Number "),
        'active' :fields.boolean("Active"),
        'pos_voucher_ids':fields.one2many('pos.voucher.line','pos_voucher_id',string="Voucher Number"),
        'company_id': fields.many2one('res.company','Company', required=False),
        'voucher_type': fields.selection([('percent', 'Percent'),
                                   ('amount', 'Amount'),
                                    ],
                                  'Voucher Type', ),
        'voucher_value':fields.float('Value'),

    }
    _defaults = {
        'is_use_number': True,
        'active': True,
    }

pos_voucher()
class pos_voucher_line(osv.osv):
    _name = 'pos.voucher.line' 
    _description = 'POS Voucher And Coupon Promotion Configuration'
    _columns = {
        'name': fields.char('Voucher Number', size=80,required=True),
        'date_use':fields.date("Date",required=False),
        'is_use' :fields.boolean("Already Used ",readonly=True),
        'pos_voucher_id':fields.many2one('pos.voucher','Voucher Type'),
        'pos_order_id':fields.many2one('pos.order','Order ID'),
        'receipt_ref' : fields.related('pos_order_id', 'receipt_ref', type="char",  string="Receipt Ref"),
        }
    _defaults = {
        'is_use': False,
    }
pos_voucher_line();