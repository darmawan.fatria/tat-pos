/******************************************************************************
 *    Point Of Sale - POS Advance
 *    Copyright (C) 2015 Telenais (http://www.telenais.com)
 *    @author Darmawan Fatriananda
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
openerp.pos_product_template = function (instance) {
    module = instance.point_of_sale;
    var QWeb = instance.web.qweb;
    var _t = instance.web._t;
/* ********************************************************
Overload: point_of_sale.PosWidget

- Add a new PopUp 'SelectVariantPopupWidget';
*********************************************************** */
    module.PosWidget = module.PosWidget.extend({

        /* Overload Section */
        build_widgets: function(){
            this._super();
            this.return_product_popup = new module.ReturnProductPopupWidget(this, {});
            this.return_product_popup.appendTo(this.$el);
            this.return_product_popup.popup_set['return-product-popup'] = this.return_product_popup;
             this.return_product_popup.hide();
        },
    });
/* ******************************************************** */
    module.ReturnProductPopupWidget = module.PopUpWidget.extend({
        template:'ReturnProductPopupWidget',
        show: function(options){
            options = options || {};
            var self = this;
            this._super();


            this.message = options.message || _t('Error');
            this.comment = options.comment || '';

            this.renderElement();

            this.$('.footer .button').click(function(){
                self.pos_widget.screen_selector.close_popup();
                if ( options.confirm ) {
                    options.confirm.call(self);
                }
            });
        },
        close:function(){
            this._super();
      
        },
    });

}